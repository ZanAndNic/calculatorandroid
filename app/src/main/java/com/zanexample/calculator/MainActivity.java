package com.zanexample.calculator;

/**
 * Проект: "Калькулятор"
 * Описание:
 * Приложение должно состоять из двух Activity, на первой Activity пользователь вводит
 * первое слагаемое, второе слагаемое и нажимает на копку “Сложить”, на втором Activity
 * пользователь видит текстовое сообщение вида “[ПЕРВОЕ_СЛАГАЕМОЕ] +
 * [ВТОРОЕ_СЛАГАЕМОЕ] = [СУММА]”.
 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}